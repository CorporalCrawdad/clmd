CFLAGS=-pthread
LIBS=$(shell curl-config --libs)

clmd: clmd.o log.o vector.o boardhash.h
	gcc -o clmd clmd.o log.o vector.o ${CFLAGS} ${LIBS}

clmd.o: clmd.c
	gcc -c clmd.c ${CFLAGS}

log.o: log.c
	gcc -c log.c ${CFLAGS}

vector.o: vector.c
	gcc -c vector.c ${CFLAGS}

boardhash.h: boardhash.in
	gperf -LANSI-C -m200 <boardhash.in >boardhash.h

test: clmd
	clmd -v -f ~/.config/qtchan/sessions/session0
	
trace: clmd
	clmd -V -f ~/.config/qtchan/sessions/session0

debug: debug/clmd
	ddd debug/clmd

debug/clmd: debug/clmd.o debug/log.o debug/vector.o
	gcc -o debug/clmd debug/clmd.o debug/log.o debug/vector.o ${CFLAGS} -g ${LIBS}

debug/clmd.o: clmd.c
	gcc -c clmd.c -o debug/clmd.o ${CFLAGS} -g

debug/log.o: log.c
	gcc -c log.c -o debug/log.o ${CFLAGS} -g

debug/vector.o: vector.c
	gcc -c vector.c -o debug/vector.o ${CFLAGS} -g
