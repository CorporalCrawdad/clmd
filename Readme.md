# C Lurk More Daemon

Daemon that will watch threads and locally archive their content.
For best results, use a viewer like qtchan.

## Options
-f \[filename]
    alternative file of threads to watch.

-af \[filename]
    additional file of threads to watch.

## Config file
In this file, threads can be specified in two ways, in both cases garbage before "4chan" and after the thread number are ignored, and can be used like comments

### Qtchan style
 - 4chan/[board]/[threadnum]

### Link style
 - https://boards.4chan.org/[board]/thread/[threadnum]
