#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>
#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <sched.h>
#include <wordexp.h>

char *argv0;
#define JSMN_STATIC
#define JSMN_STRICT
#define JSMN_PARENT_LINKS

#include "arg.h"
#include "boardhash.h"
#include "config.h"
#include "jsmn.h"
#include "log.h"
#include "vector.h"

//#define COMPOSEREMOTEFILENAME(s,t,o) t[o+13].end-t[o+13].start, s+t[o+13].start, t[o+3].end-t[o+3].start, s+t[o+3].start
//#define COMPOSELOCALFILENAME(s,t,o) t[o+1].end-t[o+1].start, s+t[o+1].start, t[o+3].end-t[o+3].start, s+t[o+3].start
//#define COMPOSELOCALFILENAME(s,t,o) TOKTOSTR(s,t,o+1), TOKTOSTR(s,t,o+3)
#define DL_IMAGES 1
#define TOKTOSTR(s, t,o) t[o].end-t[o].start, s+t[o].start

struct post {
    unsigned int num;
    unsigned int last_mod;
};

struct boardmeta {
    char board[BH_MAX_WORD_LENGTH];
    vector_t *v_posts;
};

static size_t    curl_to_fil(void*, size_t, size_t, void*);
static size_t    curl_to_vec(void*, size_t, size_t, void*);
static void      heap_freer(void*);
static int       load_config(vector_t *in, vector_t *out);
static vector_t *get_replies(const char*, int);
static int       strcmpjsmn(const char *str, const char *json, jsmntok_t, int);
static void      usage(void);
static int       update_thread(const char *board, unsigned int post, CURL *chandle);
static void      vbm_freer(void*);
static void     *watch_board(void *);

int gl_timetodie = 0;
int in_timetodie = 0;

void
catchint(int sig)
{
    if (sig == SIGINT) {
        in_timetodie=1;
        gl_timetodie=1;
        log_warn("Time to die %d", gl_timetodie);
    }
}

size_t
curl_to_fil(void *in, size_t _, size_t cnt, void *out)
{
    return fprintf((FILE *)out, "%.*s", cnt, in);
}

size_t
curl_to_vec(void *in, size_t _, size_t cnt, void *out)
{
    return vector_append(out, in, cnt);
}

void
heap_freer(void *v) 
{
    free(*(void**)v);
}

/* function load_config
takes a vector of config files and fills a vector of boardmetas with boards and their threads to watch
*/
int
load_config(vector_t *in, vector_t *out)
{
    struct boardmeta bms[BH_TOTAL_KEYWORDS];
    void *curf;
    FILE *fil;

    curf = vector_begin(in);
    fil = NULL;
    for (int i=0; i<BH_TOTAL_KEYWORDS; i++)
        bms[i].v_posts = NULL;


    while (curf) {
        if (fil = fopen(VSTR(curf), "r")) {
            char *retval;
            do {
                char readbuf[512] = {'\0'};
                vector_t *linebuf = vector_create(512, 1);
                if (retval = fgets(readbuf, 512, fil)) {
                    vector_append(linebuf, readbuf, strlen(readbuf));
                    while (*(char *)vector_end(linebuf) != '\n' && retval) {
                        retval = fgets(readbuf, sizeof(readbuf), fil);
                        vector_append(linebuf, readbuf, strlen(readbuf));
                    }
                    vector_push_back(linebuf, "\0");

                    // format is .4chan/board/num.*
                    // or .*4chan.*/board/thread/num
                    char *carr = vector_data(linebuf);
                    size_t i=0;
                    size_t t_adv=1;
                    // '.*4chan.*/'
                    /*while (strncmp("4chan", carr+i, 5) && i+5<vector_count(linebuf))
                        i++;*/
                    i = (strstr(carr, "4chan")-carr);
                    if (!(i<vector_count(linebuf))) // no 4chan found on line
                        continue;
                    i+=5;
                    if (carr[i]!='/') { // link format
                        t_adv=8;
                        while (carr[i]!='/' && i<vector_count(linebuf))
                            i++; // '/'
                    }
                    i++;
                    // 'board'
                    char bbuf[BH_MAX_WORD_LENGTH];
                    size_t j=0; 
                    while (j<BH_MAX_WORD_LENGTH && carr[i]!='/' && i<vector_count(linebuf))
                        bbuf[j++] = carr[i++];
                    bbuf[j]='\0';
                    if (in_word_set(bbuf, j)) {
                        i+=t_adv; // '/'->'\0' || 'thread/'
                        //size_t numstroff = i;
                        //while (i<vector_count(linebuf) && isdigit(carr[i]))
                        //    i++;
                        //carr[i] = '\0';
                        struct post thispost = {0, 0};
                        //thispost.num = atoi(carr+numstroff);
                        thispost.num = strtoumax(carr+i, NULL, 10);
                        if (thispost.num) {
                            log_info("Got: %s : %i", bbuf, thispost.num);
                            //vector_push_back(out, &pm);
                            int idx = hash(bbuf, j);
                            if (!bms[idx].v_posts) { // board uninitialized
                                strcpy(bms[idx].board, bbuf);
                                bms[idx].v_posts = vector_create(8, sizeof(struct post));
                            }
                            //vector_push_back(bms[idx].v_posts, &postnum);
                            vector_push_back(bms[idx].v_posts, &thispost);
                        }
                    }
                }
                vector_release(linebuf);
            } while (retval);
            fclose(fil);
        } else {
            log_error("Error with file %s", VSTR(curf));
        }
        curf = vector_next(in, curf);
    }

    for (int i=0; i<BH_TOTAL_KEYWORDS; i++) {
        if (bms[i].v_posts) { // board has entries
            vector_push_back(out, &bms[i]);
        }
    }
}

vector_t*
get_replies(const char *board, int postnum)
{
    return NULL;
}

int
strcmpjsmn(const char *str, const char *json, jsmntok_t token, int maxlen)
{
    if (token.type == JSMN_STRING) {
        if (maxlen)
            return abs(strncmp(str, json+token.start, maxlen));
        else if (strlen(str)>token.end-token.start)
            return abs(strncmp(str, json+token.start, token.end-token.start));
        else return abs(strncmp(str, json+token.start, strlen(str)));
    }
    return -1;
}

void
usage()
{
    printf("usage: %s [-vV] [-f <filename>] [-a <filename>]...\n", argv0);
}

int
update_thread(const char *board, unsigned int post, CURL *chandle)
{
    char url[128];
    CURLcode cret;
    vector_t *vjsonstr;

    vjsonstr = vector_create(1024, 1);
    vector_blank(vjsonstr);
    snprintf(url, 128, "https://a.4cdn.org/%s/thread/%d.json", board, post);
    curl_easy_setopt(chandle, CURLOPT_URL, url);
    curl_easy_setopt(chandle, CURLOPT_WRITEFUNCTION, curl_to_vec);
    curl_easy_setopt(chandle, CURLOPT_WRITEDATA, vjsonstr);
    curl_easy_setopt(chandle, CURLOPT_FAILONERROR, 1L);

    if (cret = curl_easy_perform(chandle)) {
        log_info("libcurl result from thread %u:\t%d", post, cret);
        vector_release(vjsonstr);
        return -1;
    }
    if (cret == CURLE_OK) {
        jsmn_parser jp;
        jsmntok_t  *tokens;
        char       *jsonstr;
        int tokcnt, ret=0;
        char path[64];
        sprintf(path, "%d", post);
        mkdir(path, 0777);
        chdir(path);

        jsmn_init(&jp);
        tokcnt = jsmn_parse(&jp, vector_data(vjsonstr), vector_count(vjsonstr), NULL, 0);
        if (tokcnt>0)
            tokens = malloc(sizeof(jsmntok_t)*tokcnt);
        else {
            log_error("json error");
            return 0;
        }
        jsmn_init(&jp);
        jsonstr = vector_data(vjsonstr);
        if (ret = jsmn_parse(&jp, vector_data(vjsonstr), vector_count(vjsonstr), tokens, tokcnt)<=0) {
            log_error("JSMN error: %i", ret);
        } else {
            FILE *fil = fopen("index.json", "w");
            fwrite(vector_data(vjsonstr), 1, vector_count(vjsonstr), fil);
            fclose(fil);
            if (DL_IMAGES) {
                int repcnt = tokens[2].size;
                int imgcnt = 0;
                for (int i=0; i<tokcnt && !in_timetodie; i++)
                    if (!strcmpjsmn("filename\"", jsonstr, tokens[i], 9)) {
                        imgcnt++;
                        char imgname[256];
                        int noffs = 0;
                        int foffs = 0;
                        int xoffs = 0;
                        int toffs = 0;
                        int fd;
                        FILE *fil;
                        int obj=tokens[i].parent;
                        for (int j=0; j<tokens[obj].size*2; j++) {
                            if (!strcmpjsmn("filename\"", jsonstr, tokens[obj+j], 9))
                                foffs=j+1;
                            else if (!strcmpjsmn("no\"", jsonstr, tokens[obj+j], 3))
                                noffs=j+1;
                            else if (!strcmpjsmn("ext\"", jsonstr, tokens[obj+j], 4))
                                xoffs=j+1;
                            else if (!strcmpjsmn("tim\"", jsonstr, tokens[obj+j], 4))
                                toffs=j+1;
                        }
                        if (!noffs || !foffs || !xoffs || !toffs) {
                            log_fatal("COULDN'T PARSE TOKENS %d %d %d %d in \n %d'%d'", noffs, foffs, xoffs, toffs, tokens[obj].size, strcmpjsmn("tim\"", jsonstr, tokens[obj+19], 4));
                            continue;
                        }
                        snprintf(imgname, sizeof(imgname), "%.*s-%.*s%.*s", TOKTOSTR(jsonstr, tokens, obj+noffs), TOKTOSTR(jsonstr, tokens, obj+foffs), TOKTOSTR(jsonstr, tokens, obj+xoffs));
                        if (!strcmp("ext-timew", imgname)){
                            log_fatal("EXT-TIMEW CAUGHT: offending json to follow...");
                            int badboy = tokens[tokens[i].parent].parent;
                            log_fatal("%.*s", tokens[i].end-tokens[i].start, jsonstr+tokens[i].start);
                            log_fatal("%.*s", tokens[badboy].end-tokens[badboy].start, jsonstr+tokens[badboy].start);
                        }
                        if (-1!=(fd = open(imgname, O_CREAT|O_WRONLY|O_EXCL, 0644)))
                            if (NULL!=(fil = fdopen(fd, "wb"))) {
                                snprintf(url, sizeof(url), "https://i.4cdn.org/%s/%.*s%.*s", board, TOKTOSTR(jsonstr, tokens, obj+toffs), TOKTOSTR(jsonstr, tokens, obj+xoffs));
                                curl_easy_setopt(chandle, CURLOPT_URL, url);
                                curl_easy_setopt(chandle, CURLOPT_WRITEFUNCTION, fwrite);
                                curl_easy_setopt(chandle, CURLOPT_WRITEDATA, fil);

                                if (CURLE_OK != (cret = curl_easy_perform(chandle))) {
                                    log_error("Error downloading %s: %d", url, cret);
                                }
                                fclose(fil);
                                log_info("File %s downloaded", imgname);
                                
                            } else {
                                log_error("Error fdopen(%s): %d", imgname, errno);
                                close(fd);
                            }
                        else
                            if (errno != EEXIST) 
                                log_error("Error opening %s: %d", imgname, errno);
                    }
                //log_info("Thread %d: %d replies, %d images", post, repcnt, imgcnt);
            }
        }

        chdir("..");
    } else {
        log_error("libCurl error in thread %u: %d", post, cret);
    }
    vector_release(vjsonstr);
}

void
vbm_freer(void *v)
{
    vector_release(((struct boardmeta *)v)->v_posts);
}

void *
watch_board(void *arg)
{
    struct boardmeta *pbm;
    vector_t *vjsonstr;
    vector_t *voldstr;
    CURL *chandle;
    CURLcode cret;
    char url[128];
    int i, watchcnt = 0;

    unshare(CLONE_FS);
    pbm = (struct boardmeta *)arg;
    if (!mkdir(pbm->board, 0777) || errno == EEXIST)
        if (chdir(pbm->board)) {
            log_error("chdir(%s) failed:\t%d", pbm->board, errno);
            return -1;
        }
    vjsonstr = vector_create(1024,1);
    voldstr = vector_create(1024,1);
    vector_blank(vjsonstr);
    vector_blank(voldstr);
    if (!(pbm && vjsonstr))
        return 1;
    
#ifdef DEBUG_BOARD_LOG
    snprintf(url, sizeof(url), "%s.log", pbm->board);
    FILE *log = fopen(url, "a");
    fprintf(log, "Starting logging...\n");
#endif //DEBUG_LOG
    while (vector_count(pbm->v_posts) && !in_timetodie) {
        chandle = curl_easy_init();
        snprintf(url, sizeof(url), "https://a.4cdn.org/%s/threads.json", pbm->board);
        curl_easy_setopt(chandle, CURLOPT_URL, url);
        curl_easy_setopt(chandle, CURLOPT_WRITEFUNCTION, curl_to_vec);
        curl_easy_setopt(chandle, CURLOPT_WRITEDATA, vjsonstr);
        curl_easy_setopt(chandle, CURLOPT_FAILONERROR, 1L);

        cret = curl_easy_perform(chandle);
        if(!strcmp(VSTR(voldstr), VSTR(vjsonstr))) {
            log_warn("Board %s not updated in %d seconds...", pbm->board, UPDT_INTERVAL);
        }
#ifdef DEBUG_BOARD_LOG
        fprintf(log, "Downloaded from %s\n****\n%s\n****\n", url, vector_data(vjsonstr));
        fflush(log);
#endif //DEBUG_LOG
        if (cret != CURLE_OK) {
            log_error("Couldn't download %s: %d", url, cret);
            break;
        }
        jsmn_parser jp;
        jsmntok_t *tokens;
        struct post fetchedposts[300];
        int tokcnt, ret=0;
        unsigned int postcnt= 0;

        //log_info("Watching %d threads on %s...", vector_count(pbm->v_posts), pbm->board);
        jsmn_init(&jp);
        tokcnt = jsmn_parse(&jp, VSTR(vjsonstr), vector_count(vjsonstr), NULL, 0);
        tokens = malloc(sizeof(jsmntok_t)*tokcnt);
        jsmn_init(&jp);
        if ((ret = jsmn_parse(&jp, VSTR(vjsonstr), vector_count(vjsonstr), tokens, tokcnt))<=0) {
            log_error("JSMN error: %i", ret);
        } else {
            char *jsonstr = VSTR(vjsonstr);
            for (int curtok=0; curtok<tokcnt && postcnt<300; curtok++) {
                if (!strcmpjsmn("no", jsonstr, tokens[curtok], 0)) {
                    fetchedposts[postcnt].num = strtoumax(jsonstr+tokens[curtok+1].start, NULL, 10);
                    fetchedposts[postcnt].last_mod = strtoumax(jsonstr+tokens[curtok+3].start, NULL, 10);
                    if (fetchedposts[postcnt].num == 0) {
                        log_fatal("%s", jsonstr+tokens[curtok+1].start);
                    }
                    postcnt++;
                }
            }
        }
        for (int i=0; i<vector_count(pbm->v_posts) && !in_timetodie; i++) {
            struct post *p = vector_at(pbm->v_posts, i);
            int found = 0;
            for (int j=0; j<300; j++) {
                if (fetchedposts[j].num == p->num)
                    found = j;
            }
            if (found) {
                if (fetchedposts[found].last_mod > p->last_mod) {
                    update_thread(pbm->board, p->num, chandle);
                    p->last_mod = fetchedposts[found].last_mod;
                    log_trace("%s/%d updated ^_^", pbm->board, p->num);
                }
                else log_trace("%s/%d unchanged skipping...", pbm->board, p->num);
            } else {
                log_info("%s/%d 404'd, stopping watch", pbm->board, p->num);
                vector_erase(pbm->v_posts, i--);
            }
        }
        if (watchcnt != vector_count(pbm->v_posts)) {
            watchcnt = vector_count(pbm->v_posts);
            log_info("Watching %d threads on %s...", watchcnt, pbm->board);
        }
        {
            vector_t *tmp = vjsonstr;
            vjsonstr = voldstr;
            voldstr = tmp;
        }
        //log_info("=========================================");
        free(tokens);
        curl_easy_cleanup(chandle);
        if (!in_timetodie)
            sleep(UPDT_INTERVAL);
    }
#ifdef DEBUG_BOARD_LOG
    fclose(log);
#endif //DEBUG_LOG
    vector_release(vjsonstr);
    vector_release(voldstr);
    return 1;
}

int
main(int argc, char* argv[])
{
    vector_t *vs_cfgpaths = NULL;
    vector_t *vbm_boards = NULL;
    vector_t *vpt_threads = NULL;
    size_t i;
    char *tmp;
    vs_cfgpaths = vector_create(8, sizeof(char*));
    tmp = ".threadlist";

    vector_push_back(vs_cfgpaths, &tmp);

    log_set_level(LOG_WARN);

    ARGBEGIN {
    case 'f':
        tmp = EARGF(usage());
        vector_replace(vs_cfgpaths, 0, &tmp);
        break;
    case 'a':
        tmp = EARGF(usage());
        vector_push_back(vs_cfgpaths, &tmp);
        break;
    case 'v':
        log_set_level(LOG_INFO);
        break;
    case 'V':
        log_set_level(LOG_TRACE);
        break;
    default:
        usage();
        vector_release(vs_cfgpaths);
        return -1;
    } ARGEND

    log_info("File \"%s\"", VSTR(vector_data(vs_cfgpaths)));

    for (char **s=vector_begin(vs_cfgpaths); s; s = vector_next(vs_cfgpaths, s)) {
        log_info("Using sessions file: %s", *s);
    }

    {
        struct sigaction sa;
        sa.sa_handler = catchint;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;

        sigaction(SIGINT, &sa, NULL);
    }
    while (!gl_timetodie) {
        log_info("Reinitializing...");
        vbm_boards = vector_create_deleter(128, sizeof(struct boardmeta), vbm_freer);
        load_config(vs_cfgpaths, vbm_boards);
        log_info("Watching %d boards", vector_count(vbm_boards));
        vpt_threads = vector_create(vector_count(vbm_boards), sizeof(pthread_t));
        for (struct boardmeta *pbm=vector_begin(vbm_boards); pbm; pbm=vector_next(vbm_boards, pbm)) {
            pthread_t pt;
            vector_push_back(vpt_threads, &pt);
            pthread_create(vector_end(vpt_threads), NULL, watch_board, pbm);
        }
        sleep(3*60);
        in_timetodie=1;
        for (pthread_t *ppt = vector_begin(vpt_threads); ppt; ppt = vector_next(vpt_threads, ppt)) {
            pthread_join(*ppt, NULL);
        }
        vector_release(vpt_threads);
        vpt_threads = NULL;
        vector_release(vbm_boards);
        vbm_boards = NULL;
        in_timetodie=0;
    }
    log_info("Cleaned up!");

    if (vs_cfgpaths)
        vector_release(vs_cfgpaths);
    if (vbm_boards)
        vector_release(vbm_boards);
    if (vpt_threads)
        vector_release(vpt_threads);
    pthread_exit(0);
    return 0;
}
