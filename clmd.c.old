#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <ctype.h>
#include <curl/curl.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <wordexp.h>

char *argv0;
#define JSMN_STATIC
#define JSMN_STRICT

#include "arg.h"
#include "config.h"
#include "jsmn.h"
#include "log.h"
#include "vector.h"

struct threadmeta {
    int threadnum;
    char board[BOARD_LEN];
    char chan[CHAN_LEN];
};

struct boardmeta {
    char board[BOARD_LEN];
    vector_t *i_tnums;
};

size_t curl_to_buf(void*, size_t, size_t, void*);
int    load_config(vector_t *c_watchf, vector_t *tmv);
void   heap_freer(void*);
void   vbm_freer(void*);
void   usage(void);
void * watch_board(void *arg);

size_t
curl_to_buf(void *in, size_t size, size_t cnt, void *out)
{
    return vector_append(out, in, cnt);
}

int
load_config(vector_t *c_watchf, vector_t *tmv)
{
    void *curf; 
    FILE *fil;

    curf = vector_begin(c_watchf);
    fil = NULL;

    while (curf) {
        if (fil = fopen(VSTR(curf), "r")) {
            while (!feof(fil)) {
                char readbuf[512] = {'\0'};
                vector_t *linebuf = vector_create(512, sizeof(char));
                char *retval;
                if (retval = fgets(readbuf, 512, fil)) {
                    vector_append(linebuf, readbuf, strlen(readbuf));
                    while (*(char *)vector_at(linebuf, vector_count(linebuf)-1) != '\n' && retval) {
                        retval = fgets(readbuf, sizeof(readbuf), fil);
                        vector_append(linebuf, readbuf, strlen(readbuf));
                    }
                    vector_push_back(linebuf, "\0");

                    struct threadmeta *tm = malloc(sizeof(struct threadmeta));
                    // format is [whitespace]*chan/board/num[garbage]
                    char *carr = vector_data(linebuf);
                    size_t i=0;
                    // whitespace
                    while(!isalnum(carr[i]) && i<strlen(carr))
                        i++;
                    // chan
                    unsigned int j = 0;
                    while (carr[i] != '/' && isalnum(carr[i]) && j<CHAN_LEN) {
                        tm->chan[j++] = carr[i++];
                    }
                    tm->chan[j] = '\0';
                    // /
                    i++;
                    // board
                    j=0;
                    while (carr[i] != '/' && isalnum(carr[i]) && j<BOARD_LEN) {
                        tm->board[j++] = carr[i++];
                    }
                    tm->board[j] = '\0';
                    i++;
                    const char *numstr = &(carr[i]);
                    for (; i<strlen(carr); i++) {
                        if (isdigit(carr[i]))
                            continue;
                        carr[i] = '\0';
                        break;
                    }
                    tm->threadnum = atoi(numstr);
                    if(tm->threadnum) {
                        vector_push_back(tmv, &tm);
                        log_debug("Thread loc: %p", tm);
                        log_debug("chan:%s", tm->chan);
                        log_debug("board:%s", tm->board);
                        log_debug("no:%i\n", tm->threadnum);
                    }
                    else
                        free(tm);
                }
                vector_release(linebuf);
            }
            fclose(fil);
        } else {
            log_error("Error with file %s", VSTR(curf));
        }
        curf = vector_next(c_watchf, curf);
    }
}

void
heap_freer (void *v)
{
    //struct threadmeta *tmp = *(struct threadmeta **)v;
    void *tmp = *(void **)v;
    //free(tmp);
    free(tmp);
}

void
vbm_freer (void *v)
{
    struct boardmeta *tmp = (struct boardmeta *)v;
    vector_release(tmp->i_tnums);
}

void
usage()
{
    printf("usage: %s [-vV] [-f <filename>] [-a <filename>]...\n", argv0);
}

int
update_thread(int dirfd, vector_t *vjson) 
{
    return 0;
}

void *
watch_board(void *arg)
{
    struct boardmeta *pbm;
    vector_t *vjsonstr;
    CURL *chandle;
    CURLcode ret;
    char url[128];
    int dirfd;

    struct tdata {
        int no;
        int last_modified;
        int replies;
    };

    pbm = (struct boardmeta*) arg;
    dirfd = open(pbm->board, O_RDWR | O_CREAT | O_DIRECTORY);
    chandle = curl_easy_init();
    vjsonstr = vector_create(1024, 1);
    vector_t *thinfos = vector_create(10, sizeof(struct tdata));
    if (chandle && pbm) {
        /* get json list of all threads on board */
        snprintf(url, 128, "https://a.4cdn.org/%s/threads.json", pbm->board);
        curl_easy_setopt(chandle, CURLOPT_URL, url);
        curl_easy_setopt(chandle, CURLOPT_WRITEFUNCTION, curl_to_buf);
        curl_easy_setopt(chandle, CURLOPT_WRITEDATA, vjsonstr);
        curl_easy_setopt(chandle, CURLOPT_FAILONERROR, 1l);
        
        ret = curl_easy_perform(chandle);
        /* baord 404'd? */
        if (ret == CURLE_HTTP_RETURNED_ERROR) {
            long ecode;
            curl_easy_getinfo(chandle, CURLINFO_RESPONSE_CODE, &ecode);
            if (ecode == 404L) {
                log_error("Board /%s/ returned 404", pbm->board);
                vector_release(thinfos);
                vector_release(vjsonstr);
                curl_easy_cleanup(chandle);
            }
        }
        /* parse threads.json through jsmn */
        {
            jsmn_parser jp;
            jsmntok_t  *tokens;
            int         tokcnt, ret = 0;
            jsmn_init(&jp);
            tokcnt = jsmn_parse(jp, VSTR(vjsonstr), vector_count(vjsonstr), tokens, 0);
            tokens = malloc(sizeof(jsmntok_t)*tokcnt);
            jsmn_init(&jp);
            if ((ret = jsmn_parse(jp, VSTR(vjsonstr), vector_count(vjsonstr), tokens, tokcnt))<0 ) {
                log_error("JSMN error: %i", ret);
            } else {
                
            }
        }

        /* find threadnums in pbm */
        /* compare thread last modified time to saved tmodtime */
        /* if different, update thread json and saved tmodtime */
    }
    return 1;
}

int
main(int argc, char* argv[])
{
    vector_t *c_watchf;
    vector_t *tmv;
    char *tmp;
    size_t i;
    c_watchf = vector_create(16, sizeof(char*));
    if (!c_watchf) {
        log_error("Vector allocation failed...");
        return -1;
    }
    tmp = ".threadlist";
    vector_push_back(c_watchf, &tmp);

    log_set_level(LOG_WARN);

    ARGBEGIN {
    case 'f':
        tmp = EARGF(usage());
        vector_replace(c_watchf, 0, &tmp);
        break;
    case 'a':
        tmp = EARGF(usage());
        vector_push_back(c_watchf, &tmp);
        break;
    case 'v':
        log_set_level(LOG_INFO);
        break;
    case 'V':
        log_set_level(LOG_TRACE);
        break;
    default:
        usage();
        vector_release(c_watchf);
        return -1;
    } ARGEND

    tmv = vector_create_deleter(128, sizeof(struct threadmeta*), heap_freer);
    if (!tmv) {
        log_error("Vector allocation failed...");
        vector_release(c_watchf);
        return -1;
    }
    VFORIN (c_watchf, i) {
        log_info("Using session files: %s", VSTR(vector_at(c_watchf, i)));
    }

    load_config(c_watchf, tmv);
    vector_release(c_watchf);
    log_info("Watching %i threads", vector_count(tmv));

    vector_t *boardsv = vector_create_deleter(5, sizeof(struct boardmeta), vbm_freer);
    VFORIN (tmv, i) {
        size_t idx;
        struct threadmeta *ptm;
        ptm = *(struct threadmeta **)vector_at(tmv, i);
        VFORIN (boardsv, idx) {
            struct boardmeta *pbm = vector_at(boardsv, idx);
            if (!strcmp(ptm->board, pbm->board)) {
                if (!vector_in(pbm->i_tnums, &(ptm->threadnum)))
                    vector_push_back(pbm->i_tnums, &(ptm->threadnum));
                idx = -1;
                break;
            }
        }
        if (idx!=-1) {
            struct boardmeta nbm;
            strcpy(nbm.board, ptm->board);
            nbm.i_tnums = vector_create(25, sizeof(int));
            vector_push_back(nbm.i_tnums, &(ptm->threadnum));
            vector_push_back(boardsv, &nbm);
        }
    }
    log_debug("Boards populated");
    vector_release(tmv);
    
    curl_global_init(CURL_GLOBAL_SSL);

    vector_t *vpt = vector_create(64, sizeof(pthread_t));

    VFORIN (boardsv, i) {
        struct boardmeta *pbm = vector_at(boardsv,i);
        log_info("Tracking %i\tthreads on /%s/", vector_count(pbm->i_tnums), pbm->board);
        //pthread_t *pt = malloc(sizeof(pthread_t));
        pthread_t pt;
        pthread_create(&pt, NULL, watch_board, pbm);
        vector_push_back(vpt, &pt);
    }

    VFORIN(vpt, i) {
        pthread_t *ppt = vector_at(vpt, i);
        pthread_join(*ppt, NULL);
        log_debug("thread joined");
    }

    log_debug("Cleanup time");
    curl_global_cleanup();
    vector_release(boardsv);
    vector_release(vpt);
    return 0;
}