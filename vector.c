#include "vector.h"
#include <string.h>

#ifndef GRWTH_FACTOR // allow custom growth factor
#define GRWTH_FACTOR 1.5
#endif

#define ELEM_CNT_MIN 2 
#define DEF_ELEM_CNT 8

struct vector_t{
    char* data;
    size_t elem_sz; // sizeof
    size_t elem_cnt;// elems in data
    size_t reserved; // bytes reserved in data

    vector_deleter* deleter;
};

static void
call_vector_deleter(vector_t* v, size_t from, size_t to)
{
    while (from < to) 
        v->deleter(vector_at(v, from++));
}

static void
call_all_vector_deleter(vector_t* v)
{
    call_vector_deleter(v, 0, v->elem_cnt);
}

// Allocation

vector_t*
vector_create(size_t num, size_t elem_size)
{
    if (elem_size < 0)
        return NULL;
    vector_t *out = malloc(sizeof(vector_t));
    if (out != NULL) {
        out->data = NULL;
        out->elem_cnt = 0;
        out->reserved = 0;
        out->elem_sz = elem_size;
        out->deleter = NULL;

        size_t reservcnt = (num>ELEM_CNT_MIN ? num : DEF_ELEM_CNT);
        if (!vector_realloc(out, reservcnt)) {
            free(out);
            out = NULL;
        }
    }
    return out;
}

vector_t*
vector_create_deleter(size_t num, size_t elem_size, vector_deleter* del)
{
    vector_t* out = vector_create(num, elem_size);
    if(out)
        out->deleter = del;
    return out;
}

void
vector_release(vector_t* v)
{
    if (v->deleter)
        call_all_vector_deleter(v);

    if (v->reserved)
        free(v->data);

    free(v);
}

size_t
vector_realloc(vector_t* v, size_t new_count) 
{
    size_t sz = v->elem_sz*new_count;
    char* new_data = realloc(v->data, sz);
    if (new_data) {
        v->reserved = sz;
        v->data = new_data;
        return sz;
    }
    return 0;
}

void
vector_blank(vector_t *v)
{
    memset(v->data, '\0', v->reserved);
}

// Access

void*
vector_at(vector_t* v, size_t index)
{
    return v->data + (v->elem_sz*index);
}


void*
vector_front(vector_t* v)
{
    return v->data;
}

void*
vector_back(vector_t* v)
{
    return v->data + ((v->elem_cnt-1)*v->elem_sz);
}

void*
vector_data(vector_t* v)
{
    return v->data;
}

size_t
vector_in(vector_t *v, void *query)
{
    for (size_t i=0; i<v->elem_cnt; i++) {
        if(!memcmp(vector_at(v, i), query, v->elem_sz))
            return i+1;
    }
    return 0;
}

size_t
vector_sin(vector_t *v, void *query)
{
    for (size_t i=0; i<v->elem_cnt; i++) {
        if(!strncmp(vector_at(v, i), query, v->elem_sz))
            return i;
    }
    return -1;
}

// Iterators

void*
vector_begin(vector_t* v)
{
    if (v->elem_cnt == 0)
        return NULL;
    return v->data;
}

void*
vector_end(vector_t* v)
{
    return v->data + ((v->elem_cnt-1)*v->elem_sz);
}

void*
vector_next(vector_t* v, void* e)
{
    return (((void*)v->data <= e && e < vector_end(v)) ? e+v->elem_sz : NULL);
}

// Capacity

size_t
vector_count(const vector_t* v)
{
    return v->elem_cnt;
}

size_t
vector_max_count(const vector_t* v)
{
    return v->reserved/v->elem_sz;
}

size_t
vector_reserve_count(vector_t* v, size_t new_count)
{
    if (new_count < v->elem_cnt)
        return 0;
    if (new_count == vector_max_count(v))
        return new_count;
    return vector_realloc(v, new_count);
}

size_t
vector_size(const vector_t* v)
{
    return v->elem_cnt*v->elem_sz;
}

size_t
vector_max_size(const vector_t* v)
{
    return v->reserved;
}

size_t
vector_reserve_size(vector_t* v, size_t new_size)
{
    if (new_size < v->reserved || new_size%v->elem_sz)
        return 0;
    if (new_size == v->reserved)
        return 0;
    return vector_realloc(v, new_size/v->elem_sz);
}

size_t
vector_elem_size(vector_t *v)
{
    return v->elem_sz;
}


// modification

void
vector_clear(vector_t* v)
{
    if(v->deleter)
        call_all_vector_deleter(v);
    
    v->elem_cnt=0;
}

void*
vector_insert(vector_t* v, size_t idx, const void* value)
{
    if (vector_max_count(v) > idx || vector_max_count(v) > v->elem_cnt+1) {
        if(!vector_reserve_count(v, vector_max_count(v)*GRWTH_FACTOR))
            return 0;
    }

    if(idx < v->elem_cnt && !memmove(vector_at(v, idx+1), vector_at(v, idx), v->elem_sz*v->elem_cnt-idx))
        return 0;
    
    v->elem_cnt++;
    return memcpy(vector_at(v, idx), value, v->elem_sz);
}

void*
vector_erase(vector_t* v, size_t pos)
{
    // return (pos < v->elem_cnt ? memset(vector_at(v, pos), 0, v->elem_sz) : NULL);
    if(v->deleter)
        v->deleter(vector_at(v, pos));

    if(!memmove(vector_at(v, pos), vector_at(v, pos+1), v->elem_sz*(v->elem_cnt-pos)))
        return NULL;

    v->elem_cnt--;
    return vector_at(v, pos);
}

void*
vector_erase_range(vector_t* v, size_t from, size_t to)
{
    // while(to!=from) {
    //     if(!vector_erase(v, to))
    //         return 0;
    //     --to;
    // }
    // return vector_at(v, from);
    if(v->deleter)
        call_vector_deleter(v, from, to);
    
    if(!memmove(vector_at(v,from), vector_at(v, to), v->elem_sz*(v->elem_cnt-to)))
        return NULL;
    
    v->elem_cnt -= (to-from);
    return vector_at(v, from);
}

size_t
vector_append(vector_t* v, const void* vals, size_t count)
{
/*    while(count) {
        if(!vector_insert(v, v->elem_cnt, vals++))
            break;
        --count;
    }
    return count;*/
    size_t newcnt = count + v->elem_cnt;
    if (newcnt > vector_max_count(v)) {
        size_t newrescnt = vector_max_count(v);
        do {
            newrescnt *= GRWTH_FACTOR;
        } while(newrescnt < newcnt);

        if (!vector_reserve_count(v, newrescnt))
            return 0;
    }

    memcpy(v->data + (v->elem_cnt*v->elem_sz), vals, count*v->elem_sz);
    v->elem_cnt = newcnt;
    return count;
}

size_t
vector_push_back(vector_t* v, const void* val)
{
    return vector_append(v, val, 1);
}

void
vector_pop_back(vector_t* v)
{
    if(v->deleter)
        v->deleter(vector_back(v));
    v->elem_cnt--;
}

void
vector_replace(vector_t* v, size_t idx, const void* val)
{
    if(v->deleter)
        v->deleter(vector_at(v, idx));

    if(idx<v->elem_cnt)
        memcpy(vector_at(v, idx), val, v->elem_sz);
}

void
vector_replace_range(vector_t* v, size_t idx, const void* vals, size_t count)
{
    if(v->deleter)
        call_vector_deleter(v, idx, idx+count);

    if(idx+count<v->elem_cnt)
        memcpy(vector_at(v, idx), vals, count*v->elem_sz);
}
