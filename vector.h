/******************************************************************************
#The MIT License (MIT)

Copyright (c) 2016 Skogorev Anton, 2019 Robert Stancil

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*******************************************************************************/
#include <stdlib.h>

/* convenience macro for string vectors */
#define VSTR(x) *(char **)x
#define VFORIN(v, p) for(p = vector_begin(v); p; p = vector_next(v, p))

typedef struct vector_t vector_t;
typedef void (vector_deleter)(void *);

// allocation
/* create a vector using default free() deleter*/
vector_t* vector_create   (size_t num, size_t elem_size);
/* create a vector using a custom deleter*/
vector_t* vector_create_deleter (size_t num, size_t elem_size, vector_deleter* del);
/* safely free vector and all allocated members */
void      vector_release  (vector_t*);
/* free and recreate vector members */
size_t    vector_realloc  (vector_t*, size_t);
/* zero out allocated memory */
void      vector_blank    (vector_t*);

// access
/* equivalent of C++ vector[] */
void*     vector_at   (vector_t*, size_t);
/* get first element */
void*     vector_front(vector_t*);
/* get last element */
void*     vector_back (vector_t*);
/* get pointer to vector data array */
void*     vector_data (vector_t*);
/* see if pointed data is in vector, idx+1 on success */
size_t    vector_in   (vector_t*, void *);
/* see if pointed str is in vector, idx+1 on success */
size_t    vector_sin   (vector_t*, void *);

// iterators
/* get pointer to first element */
void*     vector_begin(vector_t*);
/* get pointer to last element */
void*     vector_end  (vector_t*);
/* query what element is after provided element */
void*     vector_next (vector_t*, void*);

// capacity
/* count of how many members vector contains */
size_t    vector_count    (const vector_t*);
/* count of how many members vector can potentially contain */
size_t    vector_max_count(const vector_t*);
/* count of how many members vector currently has reserved space to hold */
size_t    vector_reserve_count(vector_t* vector, size_t new_count);
/* number of bytes currently stored by vector */
size_t    vector_size     (const vector_t*);
/* max size of vector in bytes */
size_t    vector_max_size (const vector_t*);
/* current amount of bytes reserved by vector */
size_t    vector_reserve_size(vector_t* vector, size_t new_size);
/* size of each element in vector */
size_t    vector_elem_size(vector_t* vector);

// modification
/* delete all elements in vector */
void      vector_clear    (vector_t*);
/* insert an element at an index, pushes back all subsequent elements */
void*     vector_insert   (vector_t* v, size_t idx, const void* value);
/* delete an element at an index */
void*     vector_erase (vector_t*, size_t);
/* delete a range of elements */
void*     vector_erase_range(vector_t* v, size_t from, size_t to);
/* append a range of elements to the vector */
size_t    vector_append(vector_t* v, const void* vals, size_t count);
/* push back a single value on the vector */
size_t    vector_push_back(vector_t* v, const void* val);
/* delete the last value on the vector */
void      vector_pop_back(vector_t* v);
/* replace an element at an index with an element */
void      vector_replace(vector_t* v, size_t idx, const void* val);
/* replace a range of elements with an array of elements */
void      vector_replace_range(vector_t* v, size_t idx, const void* vals, size_t count);